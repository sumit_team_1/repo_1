﻿using System;
using WebApp.BusinessLogic.Logging.Interfaces;

namespace WebApp.BusinessLogic.Logging
{
    /// <summary>
    /// For saving error logs
    /// </summary>
    public class LoggingManager:ILoggingManager
    {
        /// <summary>
        /// Logs an error
        /// </summary>
        /// <param name="text">error message</param>
        public void Log(string text)
        {
            //For demonstration purposes. 
            //You may change to log to database, Log4Net, or use cloud storage
            throw new NotImplementedException();
        }
    }
}
