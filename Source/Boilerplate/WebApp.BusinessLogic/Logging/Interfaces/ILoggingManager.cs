﻿namespace WebApp.BusinessLogic.Logging.Interfaces
{
    public interface ILoggingManager
    {
        void Log(string message);
    }
}
