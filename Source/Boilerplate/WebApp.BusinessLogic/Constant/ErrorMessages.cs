﻿namespace WebApp.BusinessLogic.Constant
{
    public class ErrorMessages
    {
        public const string GenericError = "An error has occurred";
        public const string Separator = ",";
    }
}
