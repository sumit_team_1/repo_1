﻿using System.Configuration;
using WebApp.BusinessLogic.Configuration.Interfaces;

namespace WebApp.BusinessLogic.Configuration
{
    public class ConfigurationManagerWrapper:IConfiguration
    {
        public string GetAppSettings(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}
