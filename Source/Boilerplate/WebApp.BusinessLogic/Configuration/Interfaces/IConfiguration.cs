﻿namespace WebApp.BusinessLogic.Configuration.Interfaces
{
    public interface IConfiguration
    {
        string GetAppSettings(string key);
    }
}
