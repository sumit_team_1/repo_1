﻿using System;

namespace WebApp.BusinessLogic.Extensions
{
    public static class ExceptionExtensions
    {
        public static string ToDescription(this Exception exception)
        {
            return exception.Message + "\r\n" + exception.StackTrace;
        }
    }
}
