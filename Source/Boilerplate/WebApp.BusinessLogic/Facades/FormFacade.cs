﻿using System;
using WebApp.BusinessLogic.Constant;
using WebApp.BusinessLogic.Extensions;
using WebApp.BusinessLogic.Facades.Interfaces;
using WebApp.BusinessLogic.Form.Interfaces;
using WebApp.BusinessLogic.Logging.Interfaces;
using WebApp.BusinessLogic.Models.API;
using WebApp.BusinessLogic.Models.API.Form;

namespace WebApp.BusinessLogic.Facades
{
    /// <summary>
    /// Facade layers are used to bring together underlying business logic
    /// </summary>
    public class FormFacade:IFormFacade
    {
        //Dependency injection
        public IFormLogger FormLogger { get; set; }
        public ILoggingManager LoggingManager { get; set; }

        public ApiResponse<FormResponse> LogForm(FormRequest request)
        {
            try
            {
                var result = FormLogger.LogForm(request);
                return new ApiResponse<FormResponse>
                {
                    Data = result,
                    Success = true
                };
            }
            catch (Exception ex)
            {
                LoggingManager.Log(string.Join(nameof(FormFacade), nameof(LogForm), ex.ToDescription()));
                return new ApiResponse<FormResponse>
                {
                    Message = ErrorMessages.GenericError,
                    Success = false
                };
            }
        }
    }
}
