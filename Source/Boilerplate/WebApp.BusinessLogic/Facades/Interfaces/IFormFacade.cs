﻿using WebApp.BusinessLogic.Models.API;
using WebApp.BusinessLogic.Models.API.Form;

namespace WebApp.BusinessLogic.Facades.Interfaces
{
    public interface IFormFacade
    {
        ApiResponse<FormResponse> LogForm(FormRequest request);
    }
}
