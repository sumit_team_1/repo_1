﻿namespace WebApp.BusinessLogic.Models.API.Form
{
    public class FormRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}
