﻿using WebApp.BusinessLogic.Models.API.Form;

namespace WebApp.BusinessLogic.Form.Interfaces
{
    public interface IFormLogger
    {
        FormResponse LogForm(FormRequest request);
    }
}
