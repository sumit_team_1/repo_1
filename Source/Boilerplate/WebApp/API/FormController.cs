﻿using System;
using System.Web.Http;
using WebApp.BusinessLogic.Facades.Interfaces;
using WebApp.BusinessLogic.Models.API;
using WebApp.BusinessLogic.Models.API.Form;

namespace WebApp.API
{
    /// <summary>
    /// Controllers need to be as light as possible and not contain business logic
    /// </summary>
    public class FormController : ApiController
    {
        //This is injected by dependency injection
        public IFormFacade FormFacade { get; set; }

        public ApiResponse<FormResponse> LogForm(FormRequest request)
        {
            return FormFacade.LogForm(request);
        }
    }
}
