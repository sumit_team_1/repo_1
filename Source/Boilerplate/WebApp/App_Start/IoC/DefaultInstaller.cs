﻿using System.Web.Http;
using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using WebApp.BusinessLogic.Configuration;

namespace WebApp.App_Start.IoC
{
    public class DefaultInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            RegisterExternalDependencies(container);
            RegisterControllers(container);

            //this line resolves all dependencies in this project automatically
            //But must follow naming conventions
            //NameOfClass:INameOfClass
            //It will not automatically resolve OtherNameOfClass:INameOfClass - this will need to be resolved manually in RegisterExternalDependencies
            container.Register(Classes.FromAssemblyInThisApplication().Pick().WithServiceDefaultInterfaces().LifestyleSingleton());
        }

        public void RegisterControllers(IWindsorContainer container)
        {
            container.Register(Classes.FromThisAssembly()
                .BasedOn<Controller>()
                .LifestyleTransient());

            container.Register(Classes.FromThisAssembly()
                .BasedOn<ApiController>()
                .LifestylePerWebRequest());
        }

        public void RegisterExternalDependencies(IWindsorContainer container)
        {
            container.Register(Component.For<WebApp.BusinessLogic.Configuration.Interfaces.IConfiguration>().ImplementedBy<ConfigurationManagerWrapper>());
        }
    }
}