﻿namespace WebApp.BusinessLogic.Tests.Interfaces
{
    public interface ITestBase
    {
        void Initialise();
    }
}
