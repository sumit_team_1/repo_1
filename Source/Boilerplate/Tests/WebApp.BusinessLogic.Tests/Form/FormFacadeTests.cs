﻿using System;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApp.BusinessLogic.Facades;
using WebApp.BusinessLogic.Form.Interfaces;
using WebApp.BusinessLogic.Logging.Interfaces;
using WebApp.BusinessLogic.Models.API.Form;
using WebApp.BusinessLogic.Tests.Interfaces;

namespace WebApp.BusinessLogic.Tests.Form
{
    [TestClass]
    public class FormFacadeTests:ITestBase
    {
        public IFormLogger FormLogger { get; set; }
        public ILoggingManager LoggingManager { get; set; }

        public FormFacade FormFacade { get; set; }

        [TestInitialize]
        public void Initialise()
        {
            FormLogger = A.Fake<IFormLogger>();
            LoggingManager = A.Fake<ILoggingManager>();

            FormFacade = new FormFacade
            {
                FormLogger = FormLogger,
                LoggingManager = LoggingManager
            };
        }

        [TestMethod]
        public void LogForm_ReturnsResults()
        {
            //arrange
            var request = new FormRequest
            {
                FirstName = "John",
                LastName = "Smith",
                Email = "john.smith@gmail.com"
            };
            var expectedResponse = new FormResponse
            {
                SpecialMessage = "Hello"
            };
            A.CallTo(() => FormLogger.LogForm(request)).Returns(expectedResponse);

            //act
            var response = FormFacade.LogForm(request);

            //assert
            A.CallTo(() => FormLogger.LogForm(request)).MustHaveHappened();
            Assert.AreEqual(true, response.Success);
            Assert.AreEqual(expectedResponse, response.Data);
        }

        [TestMethod]
        public void LogForm_LogsExceptionIfSomethingGoesWrong()
        {
            //arrange
            var request = new FormRequest();
            A.CallTo(() => FormLogger.LogForm(request)).Throws<Exception>().Once();

            //act
            var response = FormFacade.LogForm(request);

            //assert
            A.CallTo(() => LoggingManager.Log(A<string>.Ignored)).MustHaveHappened();
            Assert.AreEqual(false, response.Success);
        }
    }
}
